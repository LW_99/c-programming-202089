#include <stdio.h>
int main ()
{
    int n , r = 0, bal ;
    printf ("Enter a Number :");
    scanf ("%d" , &n);
    while (n !=0)
    {
        bal = n%10;
        r = r*10 + bal;
        n/= 10;
    }

    printf ("Reversed Number is %d", r);
    return 0;
}
